﻿//Norman Nguyen
//Game Manager: Where it manages your important variables like scores and players, but didn't used it for this milestone yet.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public TankController player;

	// Use this for initialization
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            {
                Debug.LogError("ERROR: There can be only be one GameManager.");
                Destroy(gameObject);
            }
        }
    }
}
