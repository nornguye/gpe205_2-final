﻿//Norman Nguyen
//This is the Angry AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngryAIController : MonoBehaviour
{
    public TankData data; //Tank Data
    AIController controller; //AI Controller
    private TankCannon cannon; //Cannon Component
    public enum AIStates //AI States: We need Chase and Flee for the Anger
    {
        Idle, Chase
    }
    public AIStates currentState; //varaible for AIStates
    public float timeInCurrentState; //Time Current State
    public float chaseDistance = 15.0f; //Chase Distance
    public float chaseTime = 15.0f; //Chase Time
    //Player Transform
    Transform playerTransform;
    //AI Transform
    Transform aiTransform;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
        cannon = GetComponent<TankCannon>();
    }
    // Update is called once per frame
    void Update()
    {
        //Current Time State match delta time.
        timeInCurrentState += Time.deltaTime;
        //Player Position which is based on the player(you)
        playerTransform = GameManager.instance.player.data.motor.transform;
        //AI Position
        aiTransform = data.motor.transform;
        //Switch Statement
        switch (currentState)
        {
            //Idle State
            case AIStates.Idle:
                Idle();
                if (Vector3.Distance(aiTransform.position, playerTransform.position) < chaseDistance)
                {
                    ChangeState(AIStates.Chase);
                }
                break;
            //Chase State
            case AIStates.Chase:
                Chase();
                FireAtPlayer();
                if (timeInCurrentState > chaseTime)
                {
                    ChangeState(AIStates.Idle);
                }
                break;
        }
    }
    //Change States for the AI States enum
    void ChangeState(AIStates newState)
    {
        //Set the state
        currentState = newState;
        //Reset the timer back to zero.
        timeInCurrentState = 0;
    }
    //IDLE = leave it be.
    void Idle()
    {

    }
    //Chase Method
    void Chase()
    {
        controller.MoveTowards(playerTransform.position);
    }
    void FireAtPlayer()
    {
        //Fire bullets at the player as it sees
        cannon.Fire();
    }
}

